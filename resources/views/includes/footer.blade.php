<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 footer-contact">
                    <h3>Calpadia Sistem Integrasi</h3>
                    <p>
                        Citilofts Sudirman 20th Fl. Unit 2015. <br />
                        Jl. K.H. Mas Mansyur<br />
                        No. 121 Jakarta 10220 Indonesia <br /><br />
                        <strong>Phone:</strong> +6221 2555-8550<br />
                        <strong>Email:</strong> marketing@calpadia.co.id<br />
                    </p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Sitemap</h4>
                    <ul>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="#about">About us</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services">Services</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="#team">Partner</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#services">Network Integrator</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#network">Network Security Solutions</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#it-services">IT Services</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#it-services">Procurement of IT Product</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#oss">Operational Support System (OSS)</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#solution">System Management</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#dms">Document Management System</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#capture">Intelligent Capture</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i>
                            <a href="/services#bpm">Business Process Management</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            Copyright &copy; 2020
            <strong><span>Calpadia Sistem Integrasi</span></strong>. All Rights Reserved
        </div>
        <div class="language">
            <a href="/"><img src="assets/img/uk.svg" class="iconuk">English <span>/</span>
                <a href="ID//"><img src="assets/img/indonesia.svg" class="iconuk"> Indonesia</a></a>
        </div>
    </div>
</footer>