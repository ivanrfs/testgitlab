<header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">
        <a href="/" class="logo mr-auto"><img src="assets/img/logo-calpadia.png" alt="" class="img-fluid" /></a>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="/#about">About</a></li>
                <li><a href="/services">Services</a></li>
                <li><a href="/#team">Partner</a></li>
                <li><a href="/#contact">Contact</a></li>
            </ul>
        </nav>
        <!-- .nav-menu -->
    </div>
</header>