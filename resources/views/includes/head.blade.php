<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />

<title>Calpadia Sistem Integrasi</title>
<meta content="" name="descriptison" />
<meta content="" name="keywords" />

<!-- flickity -->
<link rel="stylesheet" href="assets/css/flickity.css" />
<script src="assets/js/flickity.pkgd.min.js"></script>

<!-- Font Awesome -->
<script src="assets/vendor/fontawesome/fontawesome.js"></script>

<!-- Favicons -->
<link rel="shortcut icon" href="{{ asset('iconCalpadia.png') }}">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet" />
<link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet" />
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet" />
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet" />
<link href="assets/vendor/aos/aos.css" rel="stylesheet" />

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet" />