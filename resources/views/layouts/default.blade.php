<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
    @section('title', 'Calpadia Sistem Integrasi')
</head>

<body>
    @include('includes.navbar')
    @yield('content')
    @include('includes.msg')
    @include('includes.footer')
    @include('includes.ftscript')
</body>

</html>