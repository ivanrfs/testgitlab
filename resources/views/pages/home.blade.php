@extends('layouts.default')

@section('content')
<section id="hero" class="d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
                <h1>Better Solutions For Your Business</h1>
                <div class="d-lg-flex">
                    <a href="#about" class="btn-get-started scrollto">Get Started</a>
                    <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video" data-vbtype="video"
              data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a> -->
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                <img src="assets/img/why-us.png" class="img-fluid animated" alt="" />
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->

<main id="main">
    <!-- ======= Cliens Section ======= -->
    <section id="cliens" class="cliens section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="carousel" data-flickity='{ "cellAlign": "left", "autoPlay": true, "contain": true, "prevNextButtons": false, "pageDots": false }'>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-adira.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-agungsedayu.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-cimb.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-danamon.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-depkeu.png" class="img-fluid imgdep" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-dpi.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-jasaraharja.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-leapgrog.png" class="img-fluid imgleap" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-madhani.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-mandiri.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-pertamina.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-peruri.png" class="img-fluid imgperuri" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-SC.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-shopee.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-takaful.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-taspen.png" class="img-fluid imgtaspen" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-aop.png" class="img-fluid imgaop" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-adm.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-btn.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-polytron.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-bukopin.png" class="img-fluid" alt="" />
                        </div>
                        <div class="carousel-cell">
                            <img src="assets/img/Client/logo-permata.png" class="img-fluid imgpermata" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Cliens Section -->

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>About Us</h2>
                <br />
            </div>

            <div class="row content">
                <div class="col-lg-4">
                    <span class="icon1">
                        <p class="text-center"><i class="fas fa-eye"></i></p>
                    </span>
                    <h3 class="text-center">Vision</h3>
                    <h4 class="text-muted text-justify">
                        To become a leading company in providing End to End IT solution
                        for all our customers with appropriate technologies and
                        innovations.
                    </h4>
                </div>
                <div class="col-lg-4">
                    <span class="icon2">
                        <p class="text-center"><i class="fas fa-puzzle-piece"></i></p>
                    </span>
                    <h3 class="text-center">Mission</h3>
                    <h4 class="text-muted text-justify">
                        Assessing our customers needs of IT solutions based on their
                        business character and complexity. Providing the most
                        appropriate IT solutions to our customers based on the latest
                        technologies and innovations.
                    </h4>
                </div>
                <div class="col-lg-4">
                    <span class="icon3">
                        <p class="text-center"><i class="fas fa-key"></i></p>
                    </span>
                    <h3 class="text-center">Our Commitment</h3>
                    <h4 class="text-muted text-justify">
                        Build long term and sustainable relationship with all our
                        customers.<br /><br />
                        Understanding each customer's problem to implement innovative
                        and flexible IT solutions tailored to their specific needs.
                        Providing the best services through a methodological process
                        includes survey, consulting, implementation, and evaluation.
                    </h4>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us Section -->

    <!-- ======= Skills Section ======= -->
    <!-- <section id="skills" class="skills section-bg">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src="assets/img/skills.png" class="img-fluid" alt="" />
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <div class="skills-content">
              <div class="progress">
                <span class="skill">Partner<i class="val">17</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="17" aria-valuemin="10" aria-valuemax="30">
                  </div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Projects Done <i class="val">33</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">
                  </div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Years of Experience<i class="val">8</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    End Skills Section -->

    <!-- ======= Services Section ======= -->
    <!-- <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Services</h2>
        </div>

        <div class="row">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-network-chart'></i></div>
              <h4><a href="">Network Integrator</a></h4>
              <p>CSI as a System Integrator brings a number of devices to the appropriate network infrastructure, fast,
                reliable and effective.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
            data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-lock'></i></div>
              <h4><a href="">Network Security Solutions</a></h4>
              <p>As a company that embraces vendor neutral, we partner with major vendors in the IT Security sector, and
                gives us the ability to develop and implement security measures in accordance with the customer's
                business needs.</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in"
            data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4><a href="">Magni Dolores</a></h4>
              <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in"
            data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="">Nemo Enim</a></h4>
              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
            </div>
          </div>

        </div>

      </div>
    </section>End Services Section -->

    <!-- ======= Cta Section ======= -->
    <!-- <section id="cta" class="cta">
      <div class="container" data-aos="zoom-in">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3>Have Any Question?</h3>
            <p>Contact Us.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#contact">Contact Us</a>
          </div>
        </div>
      </div>
    </section>
    End Cta Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Partner</h2>
            </div>

            <div class="row">
                <div class="col-lg-3 mt-4 mt-lg-3 p-lg-4 pt-3">
                    <div class="img-fluid d-flex imgot" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/Partner/Logo_Opentext.png" class="img-fluid imgot" alt="" />
                    </div>
                </div>

                <div class="col-lg-3 mt-4 mt-lg-0 p-lg-4">
                    <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/Partner/Logo_Allot.png" class="img-fluid imgall" alt="" />
                    </div>
                </div>

                <div class="col-lg-3 mt-4 mt-lg-0 p-lg-4">
                    <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/Partner/Logo_Juniper.png" class="img-fluid" alt="" />
                    </div>
                </div>

                <div class="col-lg-3 mt-4 mt-lg-0 p-lg-4">
                    <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                        <img src="assets/img/Partner/Logo_Vmeet.png" class="img-fluid" alt="" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 mt-4 mt-lg-4 p-lg-5">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Cisco.png" class="img-fluid" alt="" />
                        </div>
                    </div>

                    <div class="col-lg-3 mt-4 mt-lg-5 p-lg-5">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Paloalto.png" class="img-fluid" alt="" />
                        </div>
                    </div>

                    <div class="col-lg-3 mt-5 mt-lg-4 p-lg-5 ml-auto mr-auto">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Impinj.png" class="img-fluid imgpin" alt="" />
                        </div>
                    </div>

                    <div class="col-lg-3 mt-5 mt-lg-3 p-lg-5 ml-auto">
                        <div class="img-fluid d-flex imgzeb" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Zebra.png" class="img-fluid imgzeb" alt="" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 mt-1 mt-lg-1">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/logo_HP.png" class="img-fluid imghp" alt="" />
                        </div>
                    </div>
                    <div class="col-lg-3 mt-3 mt-lg-3">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Dell.png" class="img-fluid imgdell" alt="" />
                        </div>
                    </div>
                    <div class="col-lg-3 mt-2 mt-lg-2">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Lenovo.png" class="img-fluid imglen" alt="" />
                        </div>
                    </div>
                    <div class="col-lg-3 mt-2 mt-lg-2">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Forescout.png" class="img-fluid imgfor" alt="" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 mt-1 mt-lg-1">
                        <div class="img-fluid d-flex imgall" data-aos="zoom-in" data-aos-delay="100">
                            <img src="assets/img/Partner/Logo_Axis.png" class="img-fluid imgax" alt="" />
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- End Team Section -->

    <!-- ======= Contact Section ======= -->

    <!-- End Contact Section -->
</main>
@stop