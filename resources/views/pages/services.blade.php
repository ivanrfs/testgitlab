@extends('layouts.services')

@section('content')
<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
            <ol>
                <li><a href="/">Home</a></li>
                <li>Services</li>
            </ol>
            <h2>Services</h2>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <section class="services-a" id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 section-title text-center" data-aos="fade-up">
                    <h2>What We Offer</h2>
                </div>

                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Network Integrator</h2>
                    <p>
                        CSI as a System Integrator brings a number of devices to the
                        appropriate network infrastructure, fast, reliable and
                        effective. This allows customers to develop their network of
                        simple connectivity today for full convergence in the future,
                        provide collaboration among employees, partners, customers, and
                        suppliers wherever and whenever needed. Technologies and
                        solutions available today include:
                    </p>
                    <p>Enterprise Network</p>
                    <ul class="a">
                        <li>Building LAN and WAN for Corporate Company or SMB</li>
                        <li>Building Network for Data Center and DRC environment</li>
                        <li>Multimedia Application</li>
                        <li>MIP enabled solution</li>
                        <li>Mobility Solution</li>
                        <li>Enterprise Security Solution</li>
                        <li>Cabling System for Wired or Optical Infrastructure</li>
                    </ul>
                    <br />

                    <p>Carrier Class Network</p>
                    <ul class="a">
                        <li>Building MPLS Networks</li>
                        <li>Building Optical Netwoks(CWDM.MAN.Metro Ethernet)</li>
                        <li>ADSL and Cable Operator Solution</li>
                        <li>IP VSAT</li>
                    </ul>
                </div>

                <div class="col-lg-4">
                    <img class="img-responsive img-centered" src="assets/img/Services/network-integrator.png" alt="Network Integrator.png" style="padding-top: 100px;" />
                </div>
            </div>
        </div>
    </section>

    <section class="services-b section-bg" id="network">
        <div class="container">
            <div class="row content">
                <div class="col-lg-4">
                    <img class="img-responsive img-centered" src="assets/img/Services/network-security-solution.png" alt="Network Security Solution" style="padding-top: 100px;" />
                </div>
                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Network Security Solutions</h2>
                    <p>
                        As a company that embraces vendor neutral, we partner with major
                        vendors in the IT Security sector, and gives us the ability to
                        develop and implement security measures in accordance with the
                        customer's business needs.
                    </p>
                    <p>We provide IT Security Services as follows:</p>
                    <ul class="a">
                        <li>Virtual Private Network Solution</li>
                        <li>Remote Access Solution</li>
                        <li>Virus Protection and Alert</li>
                        <li>Data Encryption</li>
                        <li>Bandwidth Management & Compression</li>
                        <li>Perimeter Deployment and Management</li>
                        <li>Security Policy Design and Enforcement</li>
                        <li>Security Auditing</li>
                        <li>Intrusion Detection Prevention</li>
                        <li>
                            Wireless Security and Access Control for WLAN, Hotspot and
                            WiMax
                        </li>
                        <li>
                            Single Sign On Solution based on customer application and
                            database
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="services-it" id="it-services">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">IT Services</h2>
                    <p>
                        IT services currently has a customer needs due to the complexity
                        of IT trends make it difficult to implement enterprise or manage
                        it. To bridge this gap, CSI provides solutions IT Services for
                        small, medium to large companies. Starting from project
                        implementation and maintenance of up to turn-key solution.
                    </p>
                    <p>Services IT Services we provide include:</p>
                    <ul class="a">
                        <li>Netwoks Auditing</li>
                        <li>Application Auditing</li>
                        <li>Security Auditing</li>
                        <li>Networks Implementation</li>
                        <li>Corporate Maintenance</li>
                        <li>Application Service provider</li>
                        <li>Helpdesk Support</li>
                    </ul>
                </div>

                <div class="col-lg-4">
                    <img class="img-responsive img-centered" src="assets/img/Services/it-services.png" style="padding-top: 100px;" alt="IT Services" />
                </div>
            </div>
        </div>
    </section>

    <section class="services-procure section-bg" id="procurement">
        <div class="container">
            <div class="row content">
                <div class="col-lg-4">
                    <img class="img-responsive img-centered" src="assets/img/Services/procurement-it-products.png" style="padding-top: 50px;" alt="Procurement of IT Products" />
                </div>
                <div class="col-lg-8" id="spacings">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Procurement of IT Products</h2>
                    <p>
                        Completing the solutions we have, CSI also provides procurement
                        services products.
                    </p>
                    <p>IT through purchase or leasing schemes, such as:</p>
                    <ul class="a">
                        <li>Networking Peripheral</li>
                        <li>PC Servers</li>
                        <li>Workstations and Notebooks</li>
                        <li>CCTV cameras</li>
                        <li>Video Conference</li>
                        <li>Storage</li>
                        <li>and others.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="services-oss" id="oss">
        <div class="container">
            <div class="row content">
                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Operational Support System (OSS)</h2>
                    <p>
                        OSS Architecture defines the policies, procedures, standards and
                        systems necessary to allocate costs for components and support
                        infrastructure. Emphasis on facilitating the rapid deployment of
                        shared resources while maximizing the benefits of the power of
                        funding and types of services.
                    </p>
                </div>
                <div class="col-lg-4">
                    <img class="img-responsive img-centered" src="assets/img/Services/oss.png" alt="Network Security Solution" />
                </div>
            </div>
        </div>
    </section>

    <section class="services-mng section-bg" id="solution">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <img class="img-responsive img-centered" src="assets/img/Services/solution.png" style="padding-top: 10px;" alt="IT Services" />
                </div>

                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Systems Management</h2>
                    <p>
                        Management system architecture defines how the hardware and
                        software infrastructure components will be monitored and
                        controlled. Management System covering automation and control of
                        the platform and related resources, networks and applications,
                        and the coordination and control of work flowing through the
                        system infrastructure. System focuses on issues of configuration
                        management, event log, fault detection and isolation,
                        performance measurement, and problems reporting.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="services-dms" id="dms">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Document Management System</h2>
                    <p>
                        Document Management System manages the lifecycle, distribution
                        and use of information across the organization, from capture
                        through to archiving and disposition. <br /><br />
                        Fueled by content services platforms and applications, the best
                        DMS software allows organizations to integrate the enterprise
                        processes that produce information with a central content
                        management platform, improving information access, bridging
                        isolated process siloes and ensuring governance is applied,
                        wherever and however content is created.
                    </p>
                </div>
                <div class="col-lg-4">
                    <img src="assets/img/product/dms.png" alt="Document Management System" class="img-responsive img-centered" style="padding-top: 10px;" />
                </div>
            </div>
        </div>
    </section>

    <section class="services-capture section-bg" id="capture">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Intelligent Capture</h2>
                    <p>
                        Intelligent capture automates content ingestion, speeding up the
                        routing of information to the right users and system in the
                        organization. It provides an entry point for intelligent process
                        automation (IPA) by removing unnecessary steps from users.
                        Combining standard capture features, such as optical character
                        recognition (OCR), with powerful machine learning, capture
                        extracts information from content and automatically routes it to
                        the right user and right lead system.
                    </p>
                </div>

                <div class="col-lg-4">
                    <img src="assets/img/product/capture.png" alt="Document Management System" class="img-responsive img-centered" style="padding-top: 10px;" />
                </div>
            </div>
        </div>
    </section>

    <section class="services-bpm" id="bpm">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <img src="assets/img/product/bpm.png" alt="Document Management System" class="img-responsive img-centered" style="padding-top: 10px;" />
                </div>

                <div class="col-lg-7">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Business Process Management</h2>
                    <p>
                        Business Process Management (BPM) delivers and supports a
                        variety of process-driven applications that address complex
                        business needs, while simultaneously providing a flexible
                        platform for rapidly building and deploying customer-centric
                        applications. It is often used to automate processes as part of
                        wider digital transformation initiatives.
                    </p>
                </div>
            </div>
        </div>
    </section>
</main>
@stop